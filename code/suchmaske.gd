extends Control
#suchmaske

var eintraege: Dictionary = {
	"alte werft" : {
		"titel" : "Alte Werft",
		"text" : "Die Alte Kaiserwerft",
		},
	"kanak" : {
		"titel" : "Kräne Kanak",
		"text" : "Diese Firma stellt Kräne her.",
		},
	"eisdiele amore" : {
		"titel" : "Eisdiele Amore im Alten Hafen",
		"text" : "Diese Eisdiele hat besonders leckeres Eis im Sommer.",
		},
	}

@onready var suchfeld: LineEdit = %Suchfeld
@onready var ergebnis_liste: VBoxContainer = %Ergebnisliste
@onready var text_ausgabe: Label = %Textausgabe

# Called when the node enters the scene tree for the first time.
func _ready():
	suchfeld.grab_focus()


func _on_suchfeld_text_submitted(new_text):
	var suchergebnisse: Array = textsuche(new_text)
	ergebnis_liste_erstellen(suchergebnisse)


func textsuche(suchbegriff: String) -> Array:
	var matching_keys: Array = []
	for key in eintraege:
		if suchbegriff in key.to_lower():
			matching_keys.append(key)
	return matching_keys


func ergebnis_liste_erstellen(ergebnisse: Array) -> void:
	for child in ergebnis_liste.get_children():
		child.queue_free()
	text_ausgabe.text = ""
	for e in ergebnisse:
		var ergebnis_knopf = Button.new()
		ergebnis_knopf.text = e
		ergebnis_knopf.pressed.connect(
			_on_ergebnis_button_pressed.bind(ergebnis_knopf.text)
			)
		ergebnis_liste.add_child(ergebnis_knopf)


func _on_ergebnis_button_pressed(text: String):
	text_ausgabe.text = eintraege[text.to_lower()]["text"]
