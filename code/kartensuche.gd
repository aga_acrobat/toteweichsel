extends Node
#kartensuche

var ort_liste: Array = [ "alte werft", "bulk cargo", "hafenfestung"]
var ort_sammlung: Array
var marker_scene: PackedScene = preload("res://code/karten_marker.tscn")

@onready var suchfeld: LineEdit = %Suchfeld

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	create_locations()
	suchfeld.grab_focus()


func create_locations() -> void:
	for e in ort_liste:
		var x = randi() % 1100 + 10
		var y = randi() % 600 + 50
		var ort: Dictionary = {
			position = Vector2(x, y),
			bezeichnung = e,
			}
		ort_sammlung.append(ort)


func _on_suchfeld_text_submitted(new_text:String):
	suchfeld.clear()
	remove_markers()
	for ort in ort_sammlung:
		if new_text in ort.bezeichnung:
			create_marker(ort)


func create_marker(ort: Dictionary) -> void:
	var marker: KartenMarker = marker_scene.instantiate()
	marker._ort = ort.bezeichnung
	marker.position = ort.position
	marker.add_to_group("marker")
	add_child(marker)


func remove_markers() -> void:
	var marker: Array = get_tree().get_nodes_in_group("marker")
	if marker:
		for m in marker:
			get_tree().queue_delete(m)
