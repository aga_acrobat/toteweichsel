extends Area2D
class_name KartenMarker

var _ort: String
var szene: PackedScene = preload("res://code/erste_szene.tscn")

func _on_input_event(_viewport, event, _shape_idx):
	if (
		event is InputEventMouseButton
		and event.button_index == MOUSE_BUTTON_LEFT
		and event.is_pressed()
		):
			self.on_click()


func on_click():
	get_tree().change_scene_to_packed(szene)

